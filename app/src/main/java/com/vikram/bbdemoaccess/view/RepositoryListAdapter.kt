package com.vikram.bbdemoaccess.view

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.vikram.bbdemoaccess.R
import com.vikram.bbdemoaccess.databinding.ItemRepositoryBinding
import com.vikram.bbdemoaccess.model.Value
import kotlinx.android.synthetic.main.item_repository.view.*

class RepositoryListAdapter(val repositoryList: ArrayList<Value>) :
    RecyclerView.Adapter<RepositoryListAdapter.RepositoryViewHolder>(), RepositoryClickListener {

    fun updateRepoList(newRepositoryList: List<Value>) {
        repositoryList.clear()
        repositoryList.addAll(newRepositoryList)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RepositoryViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view =
            DataBindingUtil.inflate<ItemRepositoryBinding>(
                inflater,
                R.layout.item_repository,
                parent,
                false
            )
        return RepositoryViewHolder(view)
    }

    override fun getItemCount() = repositoryList.size

    override fun onBindViewHolder(holder: RepositoryViewHolder, position: Int) {
        holder.view.repo =
            repositoryList[position]
        holder.view.listener = this

    }

    override fun onRepoClicked(v: View) {
        val uuid = v.uuid.text.toString()
        val intent = Intent(v.context, DetailPage::class.java)
        intent.putExtra("RepositoryId", uuid)
        v.context.startActivity(intent)
    }

    class RepositoryViewHolder(var view: ItemRepositoryBinding) : RecyclerView.ViewHolder(view.root)

}
package com.vikram.bbdemoaccess.view.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.vikram.bbdemoaccess.R
import com.vikram.bbdemoaccess.userData
import com.vikram.bbdemoaccess.util.SharedPreferencesHelper
import com.vikram.bbdemoaccess.view.RepositoryListAdapter
import com.vikram.bbdemoaccess.viewmodel.PageViewModel
import kotlinx.android.synthetic.main.fragment_main.*
import kotlinx.android.synthetic.main.fragment_main.view.*

/**
 * A placeholder fragment containing a simple view.
 */
class PlaceholderFragment : Fragment() {
    private val repoListAdapter = RepositoryListAdapter(arrayListOf())
    private lateinit var pageViewModel: PageViewModel
    var page = 1
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        pageViewModel = ViewModelProviders.of(this).get(PageViewModel::class.java).apply {
            setIndex(arguments?.getInt(ARG_SECTION_NUMBER) ?: 1)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_main, container, false)
        val textView: TextView = root.findViewById(R.id.section_label)
        val settingLayout: ConstraintLayout = root.findViewById(R.id.settings_layout)
        val repoLayout: SwipeRefreshLayout = root.findViewById(R.id.refreshLayout)
        pageViewModel.text.observe(this, Observer<String> {
            textView.text = it
            if (it.endsWith("1")) {
                repoLayout.visibility = View.VISIBLE
                settingLayout.visibility = View.GONE
                page = 1
            } else {
                repoLayout.visibility = View.GONE
                settingLayout.visibility = View.VISIBLE
                settingLayout.username.text = userData?.username.toString()
                settingLayout.displayname.text = userData?.displayName.toString()
                settingLayout.accounttype.text = userData?.type.toString()
                settingLayout.accountstatus.text = userData?.accountStatus.toString()
                settingLayout.createdOn.text = userData?.createdOn.toString()
                settingLayout.btn_logout.setOnClickListener(View.OnClickListener {
                    showAlertDialog()
                })
                page = 2
            }
        })
        return root
    }

    companion object {
        private const val ARG_SECTION_NUMBER = "section_number"

        @JvmStatic
        fun newInstance(sectionNumber: Int): PlaceholderFragment {
            return PlaceholderFragment().apply {
                arguments = Bundle().apply {
                    putInt(ARG_SECTION_NUMBER, sectionNumber)
                }
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        pageViewModel.refresh()
        repositoryList.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = repoListAdapter
        }

        refreshLayout.setOnRefreshListener {
            repositoryList.visibility = View.GONE
            listError.visibility = View.GONE
            loadingView.visibility = View.VISIBLE
            pageViewModel.refresh()
            refreshLayout.isRefreshing = false
        }

        observeViewModel()
    }

    fun observeViewModel() {
        pageViewModel.bbValues.observe(viewLifecycleOwner, Observer { bbValues ->
            bbValues?.let {
                repositoryList.visibility = View.VISIBLE
                repoListAdapter.updateRepoList(bbValues)
            }
        })
        pageViewModel.bbLoadingError.observe(viewLifecycleOwner, Observer { isError ->
            isError?.let {
                listError.visibility = if (it) View.VISIBLE else View.GONE
            }
        })
        pageViewModel.userDataCleared.observe(viewLifecycleOwner, Observer { isCleared ->
            isCleared?.let {
                requireActivity().finish()
            }
        })

        pageViewModel.progressLoading.observe(viewLifecycleOwner, Observer { isLoading ->
            isLoading?.let {
                loadingView.visibility = if (it) View.VISIBLE else View.GONE
                if (it) {
                    listError.visibility = View.GONE
                    repositoryList.visibility = View.GONE
                }
            }
        })
    }

    fun showAlertDialog() {
        val alertDialog: AlertDialog.Builder = AlertDialog.Builder(requireContext())
        alertDialog.setTitle("Waring")
        alertDialog.setMessage("Are you sure to logout?")
        alertDialog.setPositiveButton(
            "Yes"
        ) { _, _ ->
            pageViewModel.clearUserData()
        }
        alertDialog.setNegativeButton(
            "No"
        ) { _, _ -> }
        val alert: AlertDialog = alertDialog.create()
        alert.setCanceledOnTouchOutside(false)
        alert.show()
    }

}
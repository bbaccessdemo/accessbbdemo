package com.vikram.bbdemoaccess.view

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.textfield.TextInputEditText
import com.vikram.bbdemoaccess.R
import com.vikram.bbdemoaccess.userData
import com.vikram.bbdemoaccess.viewmodel.BBViewModel
import kotlinx.android.synthetic.main.activity_login.*


class LoginActivity : AppCompatActivity() {
    lateinit var loginBtn: Button
    lateinit var userName: TextInputEditText
    lateinit var password: TextInputEditText

    private lateinit var bbViewModel: BBViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        bbViewModel = ViewModelProviders.of(this).get(BBViewModel::class.java)
        userName = findViewById(R.id.username)
        password = findViewById(R.id.password)
        loginBtn = findViewById(R.id.btn_login)
        loginBtn.setOnClickListener(View.OnClickListener {
            if (validateInputs()) {
                setEnableControls(false)
                bbViewModel.checkLogin(userName.text.toString(), password.text.toString())
            }
        })
        observeViewModel()
        initListeners()
    }

    private fun observeViewModel() {
        bbViewModel.userDetails.observe(this, Observer { userDetails ->
            userDetails?.let {
                userData = userDetails
                setEnableControls(true)
                startActivity(Intent(this, LandingActivity::class.java))
                finish()
            }
        })
        bbViewModel.bbLoadingError.observe(this, Observer { isError ->
            isError?.let {

                setEnableControls(true)
                if (isError) {
                    userName.setText("")
                    password.setText("")
                    Toast.makeText(this, "Invalid Credentials", Toast.LENGTH_LONG).show()
                }
            }
        })

        bbViewModel.progressLoading.observe(this, Observer { isLoading ->
            isLoading?.let {
                loadingView.visibility = if (it) View.VISIBLE else View.GONE
            }
        })
    }

    private fun initListeners() {
        userName.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {}
            override fun beforeTextChanged(
                s: CharSequence, start: Int,
                count: Int, after: Int
            ) {
            }

            override fun onTextChanged(
                s: CharSequence, start: Int,
                before: Int, count: Int
            ) {
                if (s.length != 0) userName.error = null
            }
        })

        password.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {}
            override fun beforeTextChanged(
                s: CharSequence, start: Int,
                count: Int, after: Int
            ) {
            }

            override fun onTextChanged(
                s: CharSequence, start: Int,
                before: Int, count: Int
            ) {
                if (s.length != 0) password.error = null
            }
        })
    }

    private fun validateInputs(): Boolean {
        if ((username.text?.length == 0 || username.text?.toString()
                .equals("")) && (password.text?.length == 0 || password.text?.toString().equals(
                ""
            ))
        ) {
            userName.setError("Please enter your Username/Email")
            password.setError("Please enter your Password")
            return false
        } else if (username.text?.length == 0 || username.text?.toString().equals("")) {
            password.setError("Please enter your Password")
            return false
        } else if (password.text?.length == 0 || password.text?.toString().equals("")) {
            password.setError("Please enter your Password")
            return false
        } else return true
    }

    fun setEnableControls(enabled: Boolean) {
        userName.isEnabled = enabled
        password.isEnabled = enabled
        loginBtn.isEnabled = enabled
    }
}
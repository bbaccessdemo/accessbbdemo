package com.vikram.bbdemoaccess.view

import android.view.View

interface RepositoryClickListener {
    fun onRepoClicked(v: View)
}
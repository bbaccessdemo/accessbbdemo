package com.vikram.bbdemoaccess.view

import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.vikram.bbdemoaccess.R
import com.vikram.bbdemoaccess.repositoryData

class DetailPage : AppCompatActivity() {

    lateinit var type: TextView
    lateinit var Name: TextView
    lateinit var FullName: TextView
    lateinit var uuid: TextView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_page)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        type = findViewById(R.id.type)
        Name = findViewById(R.id.name)
        FullName = findViewById(R.id.fullName)
        uuid = findViewById(R.id.uuid)
        val intent = getIntent()
        val repoId = intent.getStringExtra("RepositoryId")


        for (repo in repositoryData!!) {
            if (repo.repository?.uuid.equals(repoId)) {
                type.text = repo.repository?.type.toString()
                Name.text = repo.repository?.name.toString()
                FullName.text = repo.repository?.fullName.toString()
                uuid.text = repo.repository?.uuid.toString()
                break
            }
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return true
    }
}
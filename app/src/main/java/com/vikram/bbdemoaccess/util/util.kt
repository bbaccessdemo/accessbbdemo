package com.vikram.bbdemoaccess

import android.util.Base64
import com.vikram.bbdemoaccess.model.UserData
import com.vikram.bbdemoaccess.model.Value

public var repositoryData: List<Value>? = null
public var userData: UserData? = null


fun encodeCredentials(credentials: String): String {
    return "Basic " +Base64.encodeToString(credentials.toByteArray(), Base64.NO_WRAP)
}


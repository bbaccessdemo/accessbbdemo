package com.vikram.bbdemoaccess.util

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import androidx.core.content.edit

class SharedPreferencesHelper {
    companion object {
        private const val PREF_TIME = "Pref time"
        private const val PREF_USERNAME = "Username"
        private const val PREF_PASSWORD = "Password"
        private var prefs: SharedPreferences? = null

        @Volatile
        private var instance: SharedPreferencesHelper? = null
        private val LOCK = Any()

        operator fun invoke(context: Context): SharedPreferencesHelper =
            instance ?: synchronized(LOCK) {
                instance ?: buildHelper(context).also {
                    instance = it
                }
            }

        private fun buildHelper(context: Context): SharedPreferencesHelper {
            prefs = PreferenceManager.getDefaultSharedPreferences(context)
            return SharedPreferencesHelper()
        }
    }

    fun saveUpdatedTime(time: Long) {
        prefs?.edit(commit = true) { putLong(PREF_TIME, time) }
    }

    fun getUpdatedTime() = prefs?.getLong(PREF_TIME, 0)

    fun getCacheDuration() = prefs?.getString("pref_cache_duration", "")

    fun saveUsername(username: String) {
        prefs?.edit(commit = true) { putString(PREF_USERNAME, username) }
    }

    fun getUsername() = prefs?.getString(PREF_USERNAME, "")

    fun savePassword(password: String) {
        prefs?.edit(commit = true) { putString(PREF_PASSWORD, password) }
    }

    fun getPassword() = prefs?.getString(PREF_PASSWORD, "")
}
package com.vikram.bbdemoaccess.viewmodel

import android.app.Application
import android.util.Base64
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.vikram.bbdemoaccess.encodeCredentials
import com.vikram.bbdemoaccess.model.BBApiService
import com.vikram.bbdemoaccess.model.DataResponse
import com.vikram.bbdemoaccess.model.Value
import com.vikram.bbdemoaccess.repositoryData
import com.vikram.bbdemoaccess.util.SharedPreferencesHelper
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers

class PageViewModel(application: Application) : BaseViewModel(application) {
    private val bbService = BBApiService()
    private val disposable = CompositeDisposable()

    private var prefHelper = SharedPreferencesHelper(getApplication())
    val bbValues = MutableLiveData<List<Value>>()
    val bbLoadingError = MutableLiveData<Boolean>()
    val progressLoading = MutableLiveData<Boolean>()
    val userDataCleared = MutableLiveData<Boolean>()

    private val _index = MutableLiveData<Int>()
    val text: LiveData<String> = Transformations.map(_index) {
        "Hello world from section: $it"
    }

    fun setIndex(index: Int) {
        _index.value = index
    }

    fun refresh() {
        fetchFromRemote()
    }

    private fun fetchLastFetchedDetails() {
        progressLoading.value = true
        bbValues.value = repositoryData
        bbLoadingError.value = false
        progressLoading.value = false
    }

    private fun fetchFromRemote() {
        progressLoading.value = true
        var credentials: String? = prefHelper.getUsername() + ":" + prefHelper.getPassword()

        // create Base64 encodet string
        val basic = encodeCredentials(credentials.toString())
        disposable.add(
            bbService.getRepositoryData(basic)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableSingleObserver<DataResponse>() {
                    override fun onSuccess(dataList: DataResponse) {
                        repositoryData = dataList.values
                        bbValues.value = dataList.values
                        bbLoadingError.value = false
                        progressLoading.value = false
                    }

                    override fun onError(e: Throwable) {
                        bbLoadingError.value = true
                        progressLoading.value = false
                        e.printStackTrace()
                    }
                })
        )
    }

    override fun onCleared() {
        super.onCleared()
        disposable.clear()
    }

    fun clearUserData() {
        prefHelper.saveUsername("")
        prefHelper.savePassword("")
        userDataCleared.value = true
    }
}
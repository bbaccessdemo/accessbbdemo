package com.vikram.bbdemoaccess.viewmodel

import android.app.Application
import android.util.Base64
import androidx.lifecycle.MutableLiveData
import com.vikram.bbdemoaccess.encodeCredentials
import com.vikram.bbdemoaccess.model.BBApiService
import com.vikram.bbdemoaccess.model.DataResponse
import com.vikram.bbdemoaccess.model.UserData
import com.vikram.bbdemoaccess.model.Value
import com.vikram.bbdemoaccess.repositoryData
import com.vikram.bbdemoaccess.util.SharedPreferencesHelper
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers

class BBViewModel(application: Application) : BaseViewModel(application) {
    private var prefHelper = SharedPreferencesHelper(getApplication())

    private val bbService = BBApiService()
    private val disposable = CompositeDisposable()

    val userDetails = MutableLiveData<UserData>()
    val bbLoadingError = MutableLiveData<Boolean>()
    val progressLoading = MutableLiveData<Boolean>()

    fun checkLogin(username: String, password: String) {
        progressLoading.value = true
        var credentials: String? = username + ":" + password
        // create Base64 encodet string
        val basic = encodeCredentials(credentials.toString())
        disposable.add(
            bbService.getUser(basic)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableSingleObserver<UserData>() {
                    override fun onSuccess(dataResponse: UserData) {
                        prefHelper.saveUsername(username)
                        prefHelper.savePassword(password)
                        userDetails.value = dataResponse
                        bbLoadingError.value = false
                        progressLoading.value = false
                    }

                    override fun onError(e: Throwable) {
                        bbLoadingError.value = true
                        progressLoading.value = false
                        e.printStackTrace()
                    }
                })
        )
    }


    override fun onCleared() {
        super.onCleared()
        disposable.clear()
    }
}
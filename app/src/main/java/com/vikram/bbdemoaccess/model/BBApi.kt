package com.vikram.bbdemoaccess.model

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Path


interface BBApi {
    @GET("/2.0/user")
    fun getUser(@Header("Authorization") authHeader: String): Single<UserData>

    @GET("/2.0/teams/{username}")
    fun getUserDetails(@Header("Authorization") authHeader: String,@Path("username") username: String): Single<UserData>

    @GET("/2.0/user/permissions/repositories")
    fun getRepositoryData(@Header("Authorization") authHeader: String): Single<DataResponse>
}
package com.vikram.bbdemoaccess.model

import io.reactivex.Single
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class BBApiService {
    private val BASE_URL = "https://api.bitbucket.org/"

    private val api = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .build()
        .create(BBApi::class.java)

    //repositoryData
    fun getRepositoryData(authHeader: String): Single<DataResponse> {
        return api.getRepositoryData(authHeader)
    }

    //login
    fun getUser(authHeader: String): Single<UserData> {
        return api.getUser(authHeader)
    }
}
package com.vikram.bbdemoaccess.model

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName

/*UserDetails*/
class UserData(
    @SerializedName("username") @Expose var username: String? = null,
    @SerializedName("has_2fa_enabled") @Expose  var has2faEnabled: Any? = null,
    @SerializedName("display_name") @Expose  var displayName: String? = null,
    @SerializedName("account_id") @Expose  var accountId: String? = null,
    @SerializedName("links") @Expose  var links: ExtraLinks? = null,
    @SerializedName("nickname") @Expose  var nickname: String? = null,
    @SerializedName("created_on") @Expose  var createdOn: String? = null,
    @SerializedName("is_staff") @Expose  var isStaff: Boolean? = null,
    @SerializedName("location") @Expose  var location: Any? = null,
    @SerializedName("account_status") @Expose  var accountStatus: String? = null,
    @SerializedName("type") @Expose  var type: String? = null,
    @SerializedName("uuid") @Expose  var uuid: String? = null
)

class Hooks(
    @SerializedName("href") @Expose var href: String? = null
)


class ExtraLinks(
    @SerializedName("hooks") @Expose var hooks: Hooks? = null,
    @SerializedName("self") @Expose var self: Self? = null,
    @SerializedName("repositories") @Expose var repositories: Repositories? = null,
    @SerializedName("html") @Expose var html: Html? = null,
    @SerializedName("avatar") @Expose var avatar: Avatar? = null,
    @SerializedName("snippets") @Expose var snippets: Snippets? = null
)

class Repositories(
    @SerializedName("href") @Expose var href: String? = null
)

class Snippets(
    @SerializedName("href") @Expose var href: String? = null
)

/*Repository Data*/

class Avatar__1(
    @SerializedName("href") @Expose var href: String? = null
)

class DataResponse(
    @SerializedName("pagelen") @Expose var pagelen: Int? = null,
    @SerializedName("values") @Expose var values: List<Value>? = null,
    @SerializedName("page") @Expose var page: Int? = null,
    @SerializedName("size") @Expose var size: Int? = null
)


class Html__1(
    @SerializedName("href") @Expose var href: String? = null
)

class Links(
    @SerializedName("self") @Expose var self: Self? = null,
    @SerializedName("html") @Expose var html: Html? = null,
    @SerializedName("avatar") @Expose var avatar: Avatar? = null
)

class Links__1(
    @SerializedName("self") @Expose var self: Self__1? = null,
    @SerializedName("html") @Expose var html: Html__1? = null,
    @SerializedName("avatar") @Expose var avatar: Avatar__1? = null
)

class Repository(
    @SerializedName("links") @Expose var links: Links__1? = null,
    @SerializedName("type") @Expose var type: String? = null,
    @SerializedName("name") @Expose var name: String? = null,
    @SerializedName("full_name") @Expose var fullName: String? = null,
    @SerializedName("uuid") @Expose var uuid: String? = null
)

class Self__1(
    @SerializedName("href") @Expose var href: String? = null
)

class User(
    @SerializedName("display_name") @Expose var displayName: String? = null,
    @SerializedName("uuid") @Expose var uuid: String? = null,
    @SerializedName("links") @Expose var links: Links? = null,
    @SerializedName("type") @Expose var type: String? = null,
    @SerializedName("nickname") @Expose var nickname: String? = null,
    @SerializedName("account_id") @Expose var accountId: String? = null
)

class Value(
    @SerializedName("type") @Expose var type: String? = null,
    @SerializedName("user") @Expose var user: User? = null,
    @SerializedName("repository") @Expose var repository: Repository? = null,
    @SerializedName("permission") @Expose var permission: String? = null
)

/*Common*/
class Avatar(
    @SerializedName("href") @Expose var href: String? = null
)

class Html(
    @SerializedName("href") @Expose var href: String? = null
)

class Self(
    @SerializedName("href") @Expose var href: String? = null
)